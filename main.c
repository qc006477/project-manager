#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

int changeDir() {
    char s[100];
    char dir[20];
    //takes user inputs
    printf("Please eneter the directory\n");
    scanf("%s\n", dir);
    //chnages the current directory to whatever the user inputs
    chdir(dir);
    //prints the new directory
    printf("%s\n", getcwd(s, 100));

    return 0;

}

int currentDir(){
    char s[100];
    //uses the getcwd command to get the current directory and print it
    printf("%s\n", getcwd(s, 100));
    return 0;
}

int filesInDir(void) {
  DIR *d;
  struct dirent *dir;
  d = opendir(".");
  //if theres a file in the directory, it will print it, if not, it will close the directory
  if (d) {
    while ((dir = readdir(d)) != NULL) {
      printf("%s\n", dir->d_name);
    }
    closedir(d);
  }
  return(0);
}

int removeFile(void){
  printf("Please enter the name of the file you would like to delete\n");
  char fileName[20];
  scanf("%s/n", fileName);
  //takes a file name and checks if it exists by opening it, if it does, it deletes it
  if(remove(fileName) != 0) {
    printf("File does not exist\n");
  }
  else{
    printf("File deleted successfully\n");
  }
}

int createFile(void){
  //takes an input from the user
  printf("Please eneter the file name\n");
  char fileName[20];
  scanf("%s/n", fileName);

  //checks if the file already exists, if it doesn't it creates the files, if not, it gives back an error
  if(access (fileName, F_OK ) == 0 ) {
     printf("File already exists\n");
   }

   else {
     FILE* fp = fopen(fileName, "w");
     printf("File Created\n");

  }
}

//creates a file using the name of whatever the user specifies
int createFolder(void){
  printf("Name the folder\n");
  char dirName[20];
  scanf("%s/n", dirName);

  DIR* dir = opendir(dirName);
  if(dir) {
    printf("Folder already exists\n");
  }
  //uses ENOENT to check if there is an error, if there isn't, the file doesnt exist
  else if (ENOENT == errno){
    mkdir(dirName, 0700);
    printf("Folder created\n");
  }
}

int main(){
  int running = 1;
  while( running == 1 ){

    char test[20];

    scanf("%s/n",test);

    char ls[] = "ls";
    char cd[] = "cd";
    char exit[] = "exit";
    char folder[] = "mkdir";
    char file[] = "mkfile";
    char rm[] = "rm";
    char pwd[] = "pwd";

    int resultls;
    int resultcd;
    int resultexit;
    int resultCFolder;
    int resultCFile;
    int resultRm;
    int resultPWD;

    //checks if user input is the same as "ls", "cd", "exit"
    resultls = strcmp(test, ls);
    resultcd = strcmp(test, cd);
    resultexit = strcmp(test, exit);
    resultCFolder = strcmp(test, folder);
    resultCFile = strcmp(test, file);
    resultRm = strcmp(test, rm);
    resultPWD = strcmp(test, pwd);

    //if test == ls, cd, exit
    if (resultls == 0) {
      filesInDir();
    }

    if (resultcd == 0) {
      changeDir();
    }

    if (resultCFolder == 0){
      createFolder();
    }

    if (resultCFile == 0){
      createFile();
    }

    if (resultRm == 0){
      removeFile();
    }

    if (resultPWD == 0){
      currentDir();
    }

    if (resultexit == 0) {
      running = 0;
    }
  }
}





  //menu();

  //currentDir();

  //changeDir();

  //openFile();

  //createFile();
